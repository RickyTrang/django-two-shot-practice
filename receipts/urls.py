from django.urls import path
from receipts.views import show_receipt
#import path from django.urls when you create a new urls.py
#import the list view we are creating a path for
#create a urlpatterns [] to hold the paths
urlpatterns = [
    path("", show_receipt, name="home"),
]

#After creating at least one path entry in the urlpatterns
#list of a new Django app's urls.py
# (you have to create this file yourself),
# make sure to include it in the Django project's urls.py.
