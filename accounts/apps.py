from django.apps import AppConfig


class AccountsConfig(AppConfig): # take the class name and add it into the projects settings.py
    default_auto_field = "django.db.models.BigAutoField"
    name = "accounts"
