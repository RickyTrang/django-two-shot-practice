from django import forms
#we are going to create a form so we need to import forms from django

#now we are going to create our first form
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        )
