from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
#we imported login_required from django.contrib.auth.decorators
#so we could use it to make sure that they can only see it
#if they are logged in.


# Create your views here.

@login_required #added the login_required decorator before the function
def show_receipt(request): #created a function called show_receipt, so we know its a list view
    receipts = Receipt.objects.filter(purchaser=request.user) #this gets all of the instances within the Receipt model
    #we changed the queryset of the view to filer the receipt objects where purchaser equals the logged in user
    context = {
        "receipts": receipts, #then we want to put it in the context for the template
    }
    return render(request,"receipts/receipts.html", context) #this says to render the page designated to the file_path, and the html we are giving it. then we add the context to show all the instances.

#MAKE SURE TO IMPORT THE MODEL
#NOW WE NEED TO REGISTER THIS VIEW IN THE URLS.PY
