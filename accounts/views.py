from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm

#when we create a form to login we need to add some imports
#we need to import authenticate, and login from django.contrib.auth
#we need to import User from django.contrib.auth.models
#we need to import our LoginForm from accounts.forms
#we need to import redirect from django.shortcuts


# Create your views here.
def user_login(request):        #create a list view function that displays the form when it is a get method
    if request.method == "POST":
        form = LoginForm(request.POST)#tries to log the person in when it is a post method
        if form.is_valid():#tries to log the person in when it is a post method
            username = form.cleaned_data['username']#tries to log the person in when it is a post method
            password = form.cleaned_data['password']#tries to log the person in when it is a post method

            user = authenticate(    #tries to log the person in when it is a post method
                request,
                username=username,
                password=password,
            )

            if user is not None: #if person successfully logs in it redirects the browser to "home"
                login(request,user)
                return redirect("home")
    else:
        form = LoginForm() #otherwise it sends them back to LoginForm
    context = {
        "form": form,
    }
    return render(request,"accounts/login.html", context) #renders the request, sends them to accounts/login.html with the context of the form

#since we created a list view for this function we need to
#create a templates folder in the accounts app, and in that app
#a folder named accounts to hold our login.html


#we are creating a  function for logging out
#we need to import logout from django.contrib.auth
def user_logout(request):
    logout(request)
    return redirect("login")
