from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt
#Make sure to import the models that we are registering from <app_name>.models

# Register your models here.
@admin.register(ExpenseCategory)  #we need to register the model with admin using @admin.site.register()
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


@admin.register(Account)#we need to register the model with admin using @admin.site.register()
class AccountAdmin(admin.ModelAdmin):#using the class_name and adding Admin to the end of it, we need it to inherit admin.ModelAdmin
    list_display = (#now we want it to show on our admin page with the field properties we gave it in the models.py
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)#we need to register the model with admin using @admin.site.register()
class ReceiptAdmin(admin.ModelAdmin):#using the class_name and adding Admin to the end of it, we need it to inherit admin.ModelAdmin
    list_display = (#now we want it to show on our admin page with the field properties we gave it in the models.py
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
