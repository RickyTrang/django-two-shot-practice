"""expenses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
#we imported the include module
#we imported the redirect module from django.shortcuts for our redirect page

#we are creating a function that redirects to the name of the path
#for the list view we created and designated as the home page.
def redirect_to_receipt(request):
    return redirect("home")
#after creating this function we need to add it to the url path
#since it knows we will start at the receipt page we can leave the path a empty string
#we then use the function we just created and give it the name "home"
urlpatterns = [
    path("", redirect_to_receipt, name="home"),
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),

]
# after creating the path for receipts, we need to make a html file
#for the list view we created and defined on the last line.
#But you need to create a templates folder in the app and inside
# the folder create a new folder to hold that specific app's html files.
