from django.db import models
from django.conf import settings
# Create your models here.
class ExpenseCategory(models.Model):#created a class called ExpenseCategory that inherits Model from models.
    name = models.CharField(max_length=50) #created a field for name with char length of 50
    owner = models.ForeignKey(#create a owner property that is a foreign key to the user_model
        settings.AUTH_USER_MODEL,
        related_name="categories", #related name is "categories"
        on_delete=models.CASCADE, #When the parent or receipt is deleted everything within it is deleted too.
    )
#In order to use the foreign key related to the user model, we need
#import from django.conf import settings.


class Account(models.Model):#created a class called Account that inherits Model from models.
    name = models.CharField(max_length=100)#created a field for name with char length of 100
    number = models.CharField(max_length=20)#created a field for number with char length of 20
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,#create a owner property that is a foreign key to the user_model
        related_name="accounts", #related name is "categories"
        on_delete=models.CASCADE,#When the parent or receipt is deleted everything within it is deleted too.
    )

#We use DecimalField when we want to associate money

class Receipt(models.Model):#created a class called Receipt that inherits Model from models.
    vendor = models.CharField(max_length=200)#created a field for vendor with char length of 200
    total = models.DecimalField(decimal_places=3,max_digits=10)#created a field for total with three decimal places, and max of 10 digits
    tax = models.DecimalField(decimal_places=3,max_digits=10)#created a field for tax with three decimal places, and max of 10 digits
    date = models.DateTimeField()#create a date field that contains the date and time of whent he transaction took place.
    purchaser = models.ForeignKey(#create a purchaser field that is a foreign key to the user model
        settings.AUTH_USER_MODEL,
        related_name="receipts",#create related name to "receipts"
        on_delete=models.CASCADE,#When the parent or receipt is deleted everything within it is deleted too.
    )
    category = models.ForeignKey(#create a category property that is a foreign key to the ExpenseCategory model
        ExpenseCategory,
        related_name="receipts",#create related name to "receipts"
        on_delete=models.CASCADE,#When the parent or receipt is deleted everything within it is deleted too.
    )
    account = models.ForeignKey(#create a account property that is a foreign key to the Account model
        Account,
        related_name="receipts",#create related name to "receipts"
        on_delete=models.CASCADE,#When the parent or receipt is deleted everything within it is deleted too.
        null=True,
    )
